﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using StatisticsCollector.Models;

namespace StatisticsCollector.DAL
{
    public class StatisticsStorageContext: DbContext
    {
        public DbSet<StatisticsEvent> Events { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StatisticsEvent>().HasKey(e => e.EventId);

            base.OnModelCreating(modelBuilder);
        }
    }
}