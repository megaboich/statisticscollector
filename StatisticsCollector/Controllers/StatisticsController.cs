﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using StatisticsCollector.Models;
using StatisticsCollector.DAL;
using Newtonsoft.Json;
using System.Diagnostics;

namespace StatisticsCollector.Controllers
{
    public class StatisticsController : Controller
    {
        [HttpGet]
        [OutputCache(Duration = 60)]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public ActionResult StatisticsView(string applicationName)
        {
            //Load from db
            using (var db = new StatisticsStorageContext())
            {
                Func<string, IDictionary<string, object>> getEventObj = (str =>
                {
                    var objData = (new System.Web.Script.Serialization.JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(str);
                    return objData;
                });

                var comparer = new StatisticsCollector.Models.EventClientIdComparer();

                var allData = db.Events
                    .Where(e => e.ApplicationName == applicationName)
                    .OrderByDescending(e => e.EventId)
                    .Take(10000)
                    .ToArray();

                ViewBag.AllCount = allData.Count();

                var top = allData
                    .Distinct(comparer)
                    .Select(statEvent => getEventObj(statEvent.JsonData))
                    .ToArray();

                return View(top);
            }
        }

        [HttpPost]
        public ActionResult Submit(string applicationName)
        {
            string jsonData = string.Empty;
            Request.InputStream.Position = 0;
            using (var tr = new StreamReader(Request.InputStream))
            {
                jsonData = tr.ReadToEnd();
            }

            var data = JsonConvert.DeserializeObject<dynamic>(jsonData);

            data.EventDate = DateTime.Now;

            var formattedJson = JsonConvert.SerializeObject(data, Formatting.Indented);

            var statEvent = new StatisticsEvent
            {
                ApplicationName = applicationName,
                JsonData = formattedJson,
                ClientId = Guid.Parse((string) data.ClientId),
            };

            //Save to db
            using (var db = new StatisticsStorageContext())
            {
                db.Events.Add(statEvent);
                db.SaveChanges();
            }

            return Json(new {
                processed = "Ok"
            });
        }
    }
}
