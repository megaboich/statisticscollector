﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StatisticsCollector.Models
{
    public class EventClientIdComparer: IEqualityComparer<StatisticsEvent>
    {
        public bool Equals(StatisticsEvent x, StatisticsEvent y)
        {
            return x.ClientId == y.ClientId;
        }

        public int GetHashCode(StatisticsEvent obj)
        {
            return obj.ClientId.GetHashCode();
        }
    }
}