﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StatisticsCollector.Models
{
    public class StatisticsEvent
    {
        public int EventId { get; set; }

        public Guid ClientId { get; set; }

        public string ApplicationName { get; set; }

        public string JsonData { get; set; }
    }
}